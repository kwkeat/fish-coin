//
//  FireHelp.swift
//  Fish Coin
//
//  Created by Keith Piong on 16/10/2018.
//
//

import Foundation
import FirebaseFirestore
import FirebaseAuth

class FireHelp{
    
    static let shared = FireHelp()
    
    private init() {}
    
    let db = Firestore.firestore()
    
    let user = Auth.auth().currentUser
    
    //FIRESTORE REFERENCES
    func getCoinRef() -> DocumentReference{
        let docRef = db.collection("coin").document("rxe")
        return docRef
    }
    
    func getCurrentAuthUser() -> User{
        
        //var reloadedUser:User? = nil
        let user = Auth.auth().currentUser
        //        user?.reload(completion: { (error) in
        //
        //            if error == nil{
        //                reloadedUser = user
        //            }
        //        })//end reload
        
        return user!
    }
    
    func getUserDocRef() -> DocumentReference{
        var uid:String = ""
        var docRef:DocumentReference
        
        let user = getCurrentAuthUser()
        uid = user.uid
        
        docRef = db.collection("users").document(uid)
        return docRef
    }
    
    func getCurrentPropertyRef() -> DocumentReference{
        var docRef:DocumentReference
        
        docRef = db.collection("properties").document("SNfmCkGo1leqLtmGCx6W")
        return docRef
    }
    
    func getFacilitiesColRef() -> CollectionReference{
        
        var colRef:CollectionReference
        
        let propRef = getCurrentPropertyRef()
        
        colRef = propRef.collection("facilities")
        return colRef
    }
    
    
    func getFeedColRef() -> CollectionReference{
        
        var colRef:CollectionReference
        
        colRef = db.collection("feed")
        return colRef
    }
    
    
    
}
