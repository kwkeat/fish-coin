//
//  NewsFeed.swift
//  Fish Coin
//
//  Created by Wei Keat Khor on 10/14/18.
//  Copyright © 2018 Wei Keat Khor. All rights reserved.
//

import Foundation
import UIKit

class NewsFeed {
    var image_url:String
    var title: String
    var description: String
    var date_posted:Date
    
    init(title: String, image_url: String, description:String, date_posted:Date) {
        self.title = title
        self.image_url = image_url
        self.description = description
        self.date_posted = date_posted
    }
}
