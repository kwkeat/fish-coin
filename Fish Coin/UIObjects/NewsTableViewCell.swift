 //
//  NewsTableViewCell.swift
//  Fish Coin
//
//  Created by Wei Keat Khor on 10/14/18.
//  Copyright © 2018 Wei Keat Khor. All rights reserved.
//

import UIKit
import Kingfisher
 
class NewsTableViewCell: UITableViewCell {

    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var newsDescription: UITextView!
    @IBOutlet weak var newsImage: UIImageView!
    
    func setNewsFeed(newsFeed: NewsFeed) {
        newsTitle.text = newsFeed.title
        newsDescription.text = newsFeed.description
        
        let url = URL(string: newsFeed.image_url)
    
        if (url != nil){
            newsImage.kf.setImage(with: url)
        }
    }
}
