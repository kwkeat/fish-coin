//
//  UIColor.swift
//  Fish Coin
//
//  Created by Wei Keat Khor on 10/14/18.
//  Copyright © 2018 Wei Keat Khor. All rights reserved.
//

import UIKit

extension UIColor {
    static let primaryColor = UIColor(hex: 0x3d99dd)

    convenience init(red: Int, green: Int, blue: Int, a: CGFloat = 1.0) {
        self.init(
            red: CGFloat(red) / 255.0,
            green: CGFloat(green) / 255.0,
            blue: CGFloat(blue) / 255.0,
            alpha: a
        )
    }
    
    convenience init(hex: Int, a: CGFloat = 1.0) {
        self.init(
            red: (hex >> 16) & 0xFF,
            green: (hex >> 8) & 0xFF,
            blue: hex & 0xFF,
            a: a
        )
    }
}
