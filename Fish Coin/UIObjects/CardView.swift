//
//  CardView.swift
//  Fish Coin
//
//  Created by Wei Keat Khor on 10/15/18.
//  Copyright © 2018 Wei Keat Khor. All rights reserved.
//

import UIKit

@IBDesignable class CardView: UIView {
    @IBInspectable var borderColor: UIColor = UIColor.white {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 2.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
}
