//
//  NewsViewController.swift
//  Fish Coin
//
//  Created by Wei Keat Khor on 10/14/18.
//  Copyright © 2018 Wei Keat Khor. All rights reserved.
//

import UIKit

class NewsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var feedArray = [NewsFeed]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        getFeedData()
    }
    
    func getFeedData(){
        let feedCol = FireHelp.shared.getFeedColRef();
        
        var tempArray = [NewsFeed]()
        feedCol.order(by: "feed_date_posted", descending: true).addSnapshotListener { (snapshots, error) in
            for i in (snapshots?.documents)!{
                
                let title = i.data()["feed_title"] as? String ?? ""
                let feed_description = i.data()["feed_description"] as? String ?? ""
                let image_url = i.data()["feed_image_url"] as? String ?? ""
                let datePosted = i.data()["feed_date_posted"] as? Date ?? Date()
                
                
                let newObj = NewsFeed(title: title, image_url: image_url, description: feed_description, date_posted: datePosted)
                
                tempArray.append(newObj)
            }//end loop
            
            self.feedArray = tempArray
            self.tableView.reloadData()
            self.tableView.layoutIfNeeded()
        }
    }//end function
    
    
}

extension NewsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return feedArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let newFeed = feedArray[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell") as! NewsTableViewCell
        
        cell.setNewsFeed(newsFeed: newFeed)
        
        return cell
    }
    

}
