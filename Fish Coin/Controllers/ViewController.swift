//
//  ViewController.swift
//  Fish Coin
//
//  Created by Wei Keat Khor on 10/8/18.
//  Copyright © 2018 Wei Keat Khor. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var logIn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        logIn.layer.borderWidth = 1
        logIn.layer.borderColor = UIColor.white.cgColor
    }


}

